matplotlib==3.6.2
numpy==1.23.5
python_tsp==0.3.1
scipy==1.9.3
