# UAV Path Optimisation
## Part of a project for all-Russian Hackathon 2022

Full project link: [__GitHub link__](https://github.com/brain-dash/hack/tree/view-edit)


### Overview
- Optimisation with travel salesman problem solving
- Extra points removal with Welzl algorithm and circular areas
- Path planning with Dubins curves

### Used libraries
- [numpy](https://pypi.org/project/numpy/)
- [scipy](https://pypi.org/project/scipy/)
- [matplotlib](https://pypi.org/project/matplotlib/)
- [python tsp](https://pypi.org/project/python-tsp/)  

___
### With __disabled__ optimizations:
![Figure1](/images/no-optimizations.PNG)
___
### With __enabled__ optimizations:
![Figure2](/images/all-optimizations.PNG)
___

### How to use

```python
    #points (list[list[float]]): 2D list of points coordinates 
    #vel (float): speed of UAV in km/h
    #tsp (bool): enable travel salesman optimization flag
    #wetzel (bool): enable Welzl optimization flag
    #overlapping (bool): enable overlapping points areas optimization flag

    points = [[55.763290828800116, 37.5909048461914],
              [55.769484573498744, 37.59536804199218],
              [55.77180697300689, 37.601547851562486]]

    vel = 30
    path = predict(points, vel, radius=0.005, tsp=True, wetzel=True, overlapping=True)
```



